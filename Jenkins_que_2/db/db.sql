Create database employee;
use employee;

create table employee (
    empid INTEGER PRIMARY KEY auto_increment,
    name VARCHAR(100),
    salary FLOAT, 
    age INTEGER
);