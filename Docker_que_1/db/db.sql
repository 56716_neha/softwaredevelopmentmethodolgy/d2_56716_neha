CREATE DATABASE Movie;
USE Movie;

CREATE TABLE Movie(
    movie_id INTEGER PRIMARY KEY auto_increment, 
    movie_title VARCHAR(100),
    movie_release_date DATE,
    movie_time FLOAT,
    director_name VARCHAR(100)
);