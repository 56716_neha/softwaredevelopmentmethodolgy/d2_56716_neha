const express = require('express')
const cors = require('cors')

const app = express()
app.use(cors('*'))
app.use(express.json())

const routerMovie = require('./routes/Movie.js')
app.use('./movie' , routerMovie)

app.listen(4000, '0.0.0.0', ( ) => {
    console.log("server start on port 4000")
})