const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()
//Movie(movie_id, movie_title, movie_release_date,movie_time,director_name)
//GET  --> Display Movie using name from Containerized MySQL
router.get('/' , (request,response) => {
    // const { movie_title, movie_release_date,movie_time,director_name } = request.body

    const query = `
    SELECT * FROM movie;
    `
    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

//POST --> ADD Movie data into Containerized MySQL table
router.post('/' , (request,response) => {
    const { movie_title, movie_release_date,movie_time,director_name } = request.body

    const query = `
    INSERT INTO Movie
    ( movie_title, movie_release_date,movie_time,director_name )
    VALUES
    ('${movie_title}','${movie_release_date}',${movie_time},'${director_name}');
    `
    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})
//UPDATE --> Update Release_Date and Movie_Time into Containerized MySQL table
router.put('/:movie_id' , (request,response) => {
    const { movie_release_date,movie_time } = request.body
    const { movie_id } = request.params

    const query = `
    UPDATE movie SET
    movie_release_date = '${movie_release_date}',
    movie_time = ${movie_time}
    WHERE 
    movie_id =${movie_id};
    `
    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

//DELETE --> Delete Movie from Containerized MySQL
router.delete('/:movie_id' , (request,response) => {
    //const { movie_release_date,movie_time } = request.body
    const { movie_id } = request.params

    const query = `
    DELETE FROM movie
    WHERE 
    movie_id =${movie_id};
    `
    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})
module.exports = router